//
//  CarsRequest.swift
//  SixT
//
//  Created by muralis on 9/24/17.
//  Copyright © 2017 muralis. All rights reserved.
//

import Foundation
import UIKit


let CARS_URL = "http://www.codetalk.de/cars.json"
let carIconUrlFormat =  "https://prod.drive-now-content.com/fileadmin/user_upload_global/assets/cars/%@/%@/2x/car.png"

struct CarsRequest: WSRequest {
    
 
    var method: String {
        return RequestType.GET.rawValue
    }
    
    var url: String {
        return CARS_URL
    }
    
    
    func fetchCars(_ handler: @escaping (Response<[Car]>) -> Void)  {
        
        self.sendRequest { (response) in
            
            DispatchQueue.main.async(execute: {
                switch response {
                case let Response.Success(resp: respObj):
                    
                    
                    var cars = [Car]()
                    
                    guard let jsonArray = respObj as? [[String: Any]] else {
                        handler(Response.Error(error: WSError.ServiceError))
                        return;
                    }

                    
                    for jsonObj in jsonArray{
                        
                        guard let _ = jsonObj["carImageUrl"] as? String, let color = jsonObj["color"] as? String, let fuelLevel = jsonObj["fuelLevel"] as? Double,let fuelType = jsonObj["fuelType"] as? String, let group = jsonObj["group"] as? String, let id_ = jsonObj["id"] as? String, let innerCleanliness = jsonObj["innerCleanliness"] as? String,let latitude = jsonObj["latitude"] as? Double, let licensePlate = jsonObj["licensePlate"] as? String, let longitude = jsonObj["longitude"] as? Double, let make = jsonObj["make"] as? String, let modelIdentifier = jsonObj["modelIdentifier"] as? String, let modelName = jsonObj["modelName"] as? String, let name = jsonObj["name"] as? String, let series = jsonObj["series"] as? String, let transmission = jsonObj["transmission"] as? String else { continue; }
                        
                        let carIconUrl = String(format: carIconUrlFormat, modelIdentifier, color)
                        let car = Car(id: id_, modelIdentifier: modelIdentifier, modelName: modelName, name: name, make: make, group: group, color: color, series: series, fuelType: fuelType, fuelLevel: fuelLevel, transmission: transmission, licensePlate: licensePlate, latitude: latitude, longitude: longitude, innerCleanliness: innerCleanliness, carImageUrl: carIconUrl)
                        
                        cars.append(car)
                        
                    }
                    handler(Response.Success(resp: cars))
                    
                case let Response.Error(error: error):
                    handler(Response.Error(error: error))
                    print(error)
                }
            })
        
        }
        
    }
    
}


protocol FetchCarsService { }

extension FetchCarsService where Self: UIViewController{
    
    func fetchCars(_ handler: @escaping ([Car]) -> Void ) -> Void {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate, let viewController = appDelegate.window?.rootViewController else  {
            return
        }
        viewController.showHud("")
        
        let carsRequest = CarsRequest()
        carsRequest.fetchCars { (response) in
            
            viewController.hideHUD()
            if case let Response.Success(resp: cars ) = response {
                handler(cars)
                
            }else if case let Response.Error(error: error) = response{
                viewController.displayError(error)
            }
            
        }
        
    }
}

extension MapController: FetchCarsService { }
extension ListController: FetchCarsService { }



