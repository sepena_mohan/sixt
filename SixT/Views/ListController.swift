//
//  ListControllerViewController.swift
//  SixT
//
//  Created by muralis on 9/25/17.
//  Copyright © 2017 muralis. All rights reserved.
//

import UIKit

class ListController: UITableViewController {

    var dataSrc: [Car]!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSrc = []
        self.tableView.register(UINib(nibName: ListCell.reusableIdentifier, bundle: nil), forCellReuseIdentifier: ListCell.reusableIdentifier)
        self.getCars()
        
        self.tableView.rowHeight = 60.0

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension ListController {
    
    func getCars()  {
        
        self.fetchCars { [weak self] (cars) in
            guard let weakSelf = self else { return }
            weakSelf.reloadCars(cars)
        }
    }
    
    
    func reloadCars(_ cars: [Car])  {
        self.dataSrc = cars
        self.tableView.reloadData()
    }
}


extension ListController {
    
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSrc.count
    }
    
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let tableCell = tableView.dequeueReusableCell(withIdentifier: ListCell.reusableIdentifier) as! ListCell
        tableCell.reloadCell(self.dataSrc[indexPath.row])
      
        return tableCell
    }
    
}

