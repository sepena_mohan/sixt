//
//  MapAnnotaionView.swift
//  SixT
//
//  Created by muralis on 9/25/17.
//  Copyright © 2017 muralis. All rights reserved.
//

import UIKit
import MapKit
import SDWebImage


protocol MapDrawing: class {
    func updateView()
}



extension MKAnnotationView: MapDrawing {
    
    func updateView()  {
    
        if let car = self.annotation as? Car {
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 60.0, height: 60.0))
            print(car.carImageUrl)
            let url = URL(string: car.carImageUrl)
            self.leftCalloutAccessoryView = imageView
            (self.leftCalloutAccessoryView as? UIImageView)?.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"), options: SDWebImageOptions.cacheMemoryOnly, completed: nil)
            
        }
        
        self.canShowCallout = true
        
    }
}

extension MKAnnotationView: ReusableView { }
