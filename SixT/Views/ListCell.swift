//
//  ListCell.swift
//  SixT
//
//  Created by muralis on 9/25/17.
//  Copyright © 2017 muralis. All rights reserved.
//

import UIKit
import SDWebImage

class ListCell: UITableViewCell {
    
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension ListCell {
    
    func reloadCell(_ car: Car)  {
        
        let url = URL(string: car.carImageUrl)
        self.carImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"), options: SDWebImageOptions.cacheMemoryOnly, completed: nil)
        self.titleLbl.text = car.title
        self.subTitleLbl.text = car.subtitle
        
    }
    
}

extension ListCell: ReusableView { }


