//
//  SixTRequest.swift
//  SixT
//
//  Created by muralis on 9/24/17.
//  Copyright © 2017 muralis. All rights reserved.
//


import Foundation
import SystemConfiguration

// Request timeout in seconds
let requestTimeout: TimeInterval = 2 * 60

let CARS_URL = "t http://www.codetalk.de/cars.json"


enum RequestType: String {
    case GET, POST
}


enum Response<T> {
    
    case Success(resp: T)
    case error(error: Error)
}


protocol WSRequest {
    
    var headers: [String: Any] { get }
    var method: String { get }
    var url: String { get }
    var body: [String: Any]? { get }
    
}


extension WSRequest {
    
    var headers: [String : Any] {
        return [:]
    }
    
    var body: [String: Any] {
        return [:]
    }
    
    var method: String {
        return RequestType.GET.rawValue
    }
    
    var url: String {
        return ""
    }
    
    private func buildRequest() -> URLRequest? {
        
        guard let url = URL(string: self.url) else { return nil }
        var request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalCacheData, timeoutInterval: requestTimeout)
        request.httpMethod = self.method
        
        return request
        
    }
    
    public func sendRequest(){
        
        guard let request = buildRequest() else {
            return
        }
        
        let urlSession = URLSession.shared
        let dataTask: URLSessionDataTask = urlSession.dataTask(with: request) { (data, response, error) in
            
            
            
        }
        
        dataTask.resume()
        
    }
}

protocol Connection {
    
    func isNetworkReachable() -> Bool
}


extension Connection {
    
    func isNetworkReachable() -> Bool{
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
}

protocol SixTRequest: WSRequest, Connection {
    func getData()
}
