//
//  Car.swift
//  SixT
//
//  Created by muralis on 9/24/17.
//  Copyright © 2017 muralis. All rights reserved.
//

import Foundation
import MapKit


class Car: NSObject {
    
    var id: String
    var modelIdentifier: String
    var modelName: String
    var name: String
    var make: String
    var group: String
    var color: String
    var series: String
    var fuelType: String
    var fuelLevel: Double
    var transmission: String
    var licensePlate: String
    var latitude: Double
    var longitude: Double
    var innerCleanliness: String
    var carImageUrl: String
    

    init(id: String, modelIdentifier: String, modelName: String, name: String, make: String, group: String, color: String, series: String, fuelType: String, fuelLevel: Double, transmission: String, licensePlate: String, latitude: Double, longitude: Double, innerCleanliness: String, carImageUrl: String) {
    
        self.id = id
        self.modelIdentifier = modelIdentifier
        self.modelName = modelName
        self.name = name
        self.group = group
        self.color = color
        self.series = series
        self.fuelType = fuelType
        self.fuelLevel = fuelLevel
        self.transmission = transmission
        self.licensePlate = licensePlate
        self.latitude = latitude
        self.longitude = longitude
        self.innerCleanliness = innerCleanliness
        self.carImageUrl = carImageUrl
        self.make = make
        
    
    }
    
}

extension Car: MKAnnotation {
    
    public var title: String? {
        return self.name
    }
    
    public var subtitle: String? {
        return "\(self.make) \(self.licensePlate)"
    }
    
    public var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2DMake(self.latitude, self.longitude)
    }
    
}


