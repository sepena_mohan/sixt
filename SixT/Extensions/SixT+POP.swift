//
//  SixT+POP.swift
//  SixT
//
//  Created by muralis on 9/25/17.
//  Copyright © 2017 muralis. All rights reserved.
//

import Foundation
import MapKit

protocol ReusableView: class { }

extension ReusableView where Self: UIView {
    
    static var reusableIdentifier: String {
        return String(describing: self)
    }
}





