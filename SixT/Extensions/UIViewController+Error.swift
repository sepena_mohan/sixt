//
//  UIViewController+Error.swift
//  SixT
//
//  Created by muralis on 9/24/17.
//  Copyright © 2017 muralis. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD


extension UIViewController {
    
    func displayError(_ error: Error)  {
        
        let alertController = UIAlertController()
        alertController.title = "Alert"
        
        switch error {
        case WSError.DataError:
            alertController.message = "Error occured with input data."
            
        case WSError.NetWorkError:
            alertController.message = "Network Offline."
            
        case WSError.ServiceError:
            alertController.message = "Problem encounted with server, please try after sometime."
            
        default:
            alertController.message = "Please try after some."
        }
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
}

extension UIViewController {
    
    func showHud(_ message: String) {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = message
        hud.isUserInteractionEnabled = false
    }
    
    func hideHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
}
